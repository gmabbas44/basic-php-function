<?php

$n = "this is string";
if (is_string($n)) {
    echo $n;
}
echo '<br>';

$n = 250.5;
if (is_numeric($n)) {
    echo $n;
}
echo '<br>';

$n = 250;
if (is_int($n) && is_integer($n)) {
    echo $n;
}
echo '<br>';

$n = true;
if (is_bool($n)) {
    echo $n;
}
echo '<br>';

$n = 25.0425;
if (is_float($n)) {
    echo $n;
}
echo '<br>';

$n = [1,2,['a','b','c']];
if (is_array($n)) {
    print_r($n);
    echo '<br>';
    var_export($n);
}

    
echo '<br>';



?>